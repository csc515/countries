package edu.missouristate.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import edu.missouristate.domain.Country;
import edu.missouristate.service.CountryService;

@Controller
public class CountryController {
	
	@Autowired
	CountryService countryService;
	
	@GetMapping(value="/")
	public String getCountries(Model model) {
		List<Country> countryList = countryService.getCountries();
		model.addAttribute("countryList", countryList);
		return "countries.jsp";
	}
	
	
	// Browser => Request => Web Server => Dispatcher Servlet <=> Controller <=> Service <=> Repository => DB
	
}
