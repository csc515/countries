package edu.missouristate.repository;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.Country;
import edu.missouristate.domain.QCountry;

@Repository
public class CountryRepositoryImpl extends QuerydslRepositorySupport implements CountryRepositoryCustom {

	QCountry countryTable = QCountry.country1;
	
	public CountryRepositoryImpl() {
		super(Country.class);
	}

	// Object Oriented SQL goes here...
	
	
}
