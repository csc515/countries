package edu.missouristate.repository;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.domain.Country;

public interface CountryRepository extends CrudRepository<Country, Integer>, CountryRepositoryCustom {
	// Spring Data abstract methods go here
	
}
