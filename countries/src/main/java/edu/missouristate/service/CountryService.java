package edu.missouristate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Country;
import edu.missouristate.repository.CountryRepository;

@Service("countryService")
public class CountryService {

	@Autowired
	CountryRepository countryRepo;
	
	public List<Country> getCountries() {
		return (List<Country>) countryRepo.findAll();
	}

	
	// Browser => Request => Web Server => Dispatcher Servlet <=> Controller <=> Service <=> Repository => DB
	
}
