<%@ include file="/layouts/include.jsp"%>
<!doctype html>
<html lang="en">
<head>
<title>Design Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="https://www.missouristate.edu/favicon.ico" />
<link rel="stylesheet" href="resources/vendor/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/styles.css">
<script src="<%=request.getContextPath() %>/resources/vendor/js/jquery-3.4.1.slim.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/vendor/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/vendor/js/popper.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Countries</h1>
		
		<div class="row">
			<div class="col-sm-3">
				<select id="country" class="form-control">
					<c:forEach var="countryList" items="${countryList}">
						<option value="${countryList.country}">${countryList.country}</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</div>
	<script src="<%=request.getContextPath() %>/resources/js/script.js"></script>
</body>
</html>